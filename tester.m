
bdapi = 'https://bd-api-dev.ncsa.illinois.edu';
bd_uname = '';
bd_pword = '';
token = '';
convert_format = '';
input_file = 'http://browndog.ncsa.illinois.edu/examples/browndog.png';
output_path = '';

bd_funcs = bd_client;

%--- GET KEY AND TOKEN -----------------------
% [key,token] = bd_funcs.key_token(bdapi,bd_uname,bd_pword)

%--- GET OUTPUTS FROM DAP --------------------
% available_outputs = bd_funcs.outputs(bdapi,convert_format,token)

%--- CONVERT FILE USING DAP ------------------
% converted_file_location = bd_funcs.convert(bdapi,input_file,convert_format,output_path,token)

%--- DTS extract ------------------------------
% metadata = bd_funcs.extract(bdapi,input_file,token)


