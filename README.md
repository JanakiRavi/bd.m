# Matlab Client for BrownDog

This library allows a Matlab user to use BrownDog to get possible file conversion output formats, to convert files, and to run extractors.

*Tested on Matlab R2015b (8.6.0.267246) 64-bit (maci64) on Mac OS X El Capitan 10.11.3*

## Installation

#### *Clone Repository:*

```
git clone https://opensource.ncsa.illinois.edu/bitbucket/scm/bd/bd.m.git
cd bd.m
```

Start Matlab and verify that you are in bd.m directory.

### *1. Build non-persistent BrownDog Client* 
##### If you stop Matlab or clear your workspace, you will need to do this again.

From within the cloned directory:

```
bd_funcs = bd_client;
```


### *2 Create BrownDog Matlab App*

##### If you create a BrownDog Matlab App, it will persist in your environment; but, you must create the app within the bd.m directory.

From within the cloned directory, run these commands in the Matlab shell:


```
matlab.apputil.install('bd_client.mlappinstall')
```
which returns something like:

```
id: 'bd_clientAPP'
name: 'bd_client'
status: 'installed'
location: '/Users/marcuss/Documents/MATLAB/Add-Ons/Apps/bd_client'
```
#### *Add to Path*
Use the location path from above and add 'code' to the end of the path.  For example:


```
addpath('/Users/marcuss/Documents/MATLAB/Add-Ons/Apps/bd_client/code')
```

#### *Add java dependency to java path*
add /java/org.json.jar and /java/PostFile.jar to the path above.  For Example:

```
javaaddpath('/Users/marcuss/Documents/MATLAB/Add-Ons/Apps/bd_client/code/java/org.json.jar')
javaaddpath('/Users/marcuss/Documents/MATLAB/Add-Ons/Apps/bd_client/code/java/PostFile.jar')
```

#### *Load Client*

In the Matlab script where you would like to use the BrownDog Client, bring the library into the workspace by adding at the top of the script.


```
bd_funcs = bd_client;
```

##### You should now be able to call the BrownDog Matlab Client from any directory.

## Examples

#### *Get Key/Token*

```
bdapi = 'https://bd-api-dev.ncsa.illinois.edu';
bd_uname = '<Your BrownDog username>'
bd_pword = '<Your BrownDog password>'
[key, token] = bd_funcs.key_token(bdapi, bd_uname, bd_pword);
```

#### *Outputs*
Get all possible conversions for a specific file format.  Returns an array of possible output formats.

```
bdapi = 'https://bd-api-dev.ncsa.illinois.edu';
token = '<Your BrownDog API token>'

convert_format = 'png';

available_outputs = bd_funcs.outputs(bdapi,convert_format,token)

```
#### *Convert*
Convert a file to any of the formats available from outputs - path can be either local or http(s).  Downloads the file and returns the local path.

```
bdapi = 'https://bd-api-dev.ncsa.illinois.edu';
token = '<Your BrownDog API token>'

convert_format = 'png';
input_file = 'https://www.google.com/sky/about_files/messier82.jpg';
output_path = './messier82.png';

converted_file_location = bd_funcs.convert(bdapi,input_file,convert_format,output_path,token)
```
#### *Extract*
Upload a file to DTS and return extraction metadata as a JSON array.

```
bdapi = 'https://bd-api-dev.ncsa.illinois.edu';
token = '<Your BrownDog API token>'

input_file = 'https://illusionofvolition.files.wordpress.com/2013/04/mosaic.jpg'

metadata = bd_funcs.extract(bdapi,input_file,token)
```
## Uninstall
In shell:

```
matlab.apputil.uninstall('bd_clientAPP')
```

## For Developers

### Update App

If you wish to change the code and update the app, after editing code:

```
matlab.apputil.create
```
add main file bd_client.m  
add folder /java
hit button 'package'

To get list of apps:

```
matlab.apputil.getInstalledAppInfo
```

### *Some Matlab help*

View java paths:

```
javaclasspath
```

or just the dynamic java path

```
javaclasspath('-dynamic')
```

Empty Java dynamic path:

```
javarmpath('<write full path here>')
```

Add path the Java dynamic path:

```
javaaddpath('<write full path here>')
```

View directories in search path:

```
path
```

Add directory from search path:

```
addpath('<write full path here>')
```

Remone directory in search path:

```
rmpath('<write full path here>')
```


### Update java library

After updating PostFile.java:

```
javac PostFile.java
jar -cvf PostFile.jar PostFile.class
rm PostFile.class
```