%---MAIN FUNCTION THAT RETURNS (SUB)FUNCTION HANDLES-------
function bd_funcs = bd_client
    bd_funcs.outputs = @outputs;
    bd_funcs.convert = @convert;
    bd_funcs.download_file = @download_file;
    bd_funcs.extract = @extract;
    bd_funcs.key_token = @key_token;
    
    java_path = javaclasspath('-dynamic');
    json_exists = false;
    for n=1:length(java_path)
        [pathstr,name,ext] = fileparts(java_path{n});
        if strcmp(strcat(name,ext),'org.json.jar')
            json_exists = true;
            break
        end       
    end
    
    if ~json_exists
        javaaddpath('./java/org.json.jar');
        javaaddpath('./java/PostFile.jar');
    end
end

%--- DAP OUTPUTS ------------------------------------------
%   Returns available conversion outputs for a file extension
%
%   dap                 dap domain
%   extension           input file extension e.g., 'jpg'          

function dap_outputs = outputs(bdapi,extension,token)
    url = strcat(bdapi,'/v1/conversions/inputs/',extension,'?token=',token)
    dap_outputs = strsplit(getmetadata(url,token),',');
end

%--- DAP CONVERT ------------------------------------------
%   Converts file, downloads converted file, and returns path to converted file
%
%   bdapi               browndog api domain
%   input_filename      path to input file, can be local or http
%   output_extension    covert to this format e.g., 'png'
%   output_path         location to put converted file
%   vararging(1)        wait time for conversion before download
function return_string = convert(bdapi,input_filename,output_extension,output_path,token)
    switch nargin
        case 5
            wait=60;
        otherwise
            warning('Warning: too many input arguments for function convert, tailing arguments ignored');
    end
    
    url = strcat(bdapi,'/v1/conversions/',output_extension);
    
    [pathstr,name,ext] = fileparts(input_filename);
    filename = strcat(name,ext);
    
    % POST TO DAP
    url_converted = char(post_file(url,filename,input_filename, token,'convert'));

    % DOWNLOAD CONVERTED FILE
    return_string = download_file(url_converted,output_path,token,wait);
end

%--- DTS EXTRACT -------------------------------------------
%   upload file and return dts metadata
%
%   dts                 dts domain
%   input_filename      path to file - http or local
function return_string = extract(bdapi,input_filename,token,varargin)

    % GET INPUT WAIT TIME OR USE DEFAULT
    switch nargin
        case 3
            wait=60;
        case 4
            wait=varargin{1,1};
        otherwise
            warning('Warning: too many input arguments for function convert, tailing arguments ignored');
    end

    [pathstr,name,ext] = fileparts(input_filename);
    filename = strcat(name,ext);
    
    % POST THE FILE
    if strcmp(input_filename(1:4),'http')
        url = strcat(bdapi,'/v1/extractions/url');
        file_id = char(org.json.JSONObject(char(posturl(url,input_filename,token))).getString('id'));
    else
        url = strcat(bdapi,'/v1/extractions/file');
        file_id = char(org.json.JSONObject(char(post_file(url,filename,input_filename,token,'extract'))).getString('id'));
    end
    
    url_status = strcat(bdapi,'/v1/extractions/',file_id,'/status');
    
    done = 0;
    while done == 0
        status = org.json.JSONObject(char(getmetadata(url_status,token))).getString('Status');
        if strcmp(status,'Done')
            done = 1;
        end
        if wait > 0
            pause(1);
            wait = wait-1;
        else
            return_string = 'Operation timed out!';
            return;
        end      
    end

    % GET EXTRACTED DATA AFTER EXTRACTIONS ARE COMPLETE
    combined_metadata = org.json.JSONArray();
   
    disp('Getting tags');
    url_tags = strcat(bdapi,'/v1/extractions/files/',file_id,'/tags');
    try
        tags_string = getmetadata(url_tags,token);
        if strcmp(tags_string(1),'[') && strcmp(tags_string(end),']')
            tags_string(end) = '';
            tags_string(1) = '';
        end 
        if size(tags_string) > 0
            tags = org.json.JSONObject(tags_string);
            combined_metadata.put(tags);
        end
    catch
        disp('Getting tags failed - not adding tags to metadata output')
    end
        

    disp('Getting versus metadata');
    url_versus_metadata = strcat(bdapi,'/v1/extractions/files/',file_id,'/versus_metadata'); 
    try
        versus_metadata_string = getmetadata(url_versus_metadata,token);
        if strcmp(versus_metadata_string(1),'[') && strcmp(versus_metadata_string(end),']')
            versus_metadata_string(end) = '';
            versus_metadata_string(1) = '';
        end      
        if size(versus_metadata_string) > 0
            versus_metadata = org.json.JSONObject(versus_metadata_string);
            combined_metadata.put(versus_metadata);
        end 
    catch
        disp('Getting versus metadata failed - not adding versus metadata to metadata output')
    end
    
    disp('Getting metadata.json');
    url_jsonld_metadata = strcat(bdapi,'/v1/extractions/files/',file_id,'/metadata.jsonld');
    try
        jsonld_metadata_string = getmetadata(url_jsonld_metadata,token);
        if strcmp(jsonld_metadata_string(1),'[') && strcmp(jsonld_metadata_string(end),']')
            jsonld_metadata_string(end) = '';
            jsonld_metadata_string(1) = '';
        end      
        if size(jsonld_metadata_string) > 0
            jsonld_metadata = org.json.JSONObject(jsonld_metadata_string);
            combined_metadata.put(jsonld_metadata);
        end 
    catch
        disp('Getting metadata.jsonld failed - not adding versus metadata.jsonld to metadata output')
    end
    return_string = combined_metadata;
        
end

%--- DOWNLOAD FILE -----------------------------------------
%   vararging(1): wait time for conversion before download
function return_string = download_file(url,filename,token,varargin)
    switch nargin
        case 4
            wait=60;
        otherwise
            warning('Warning: too many input arguments for function convert, tailing arguments ignored');
    end
    wait_input = wait;

    url = strcat(url,'?token=',token);
 
    downloaded = 0;   
    while downloaded == 0
        try
            return_string = websave(filename,url);
            downloaded = 1;
        catch
            pause(1);
            wait = wait-1;
            if wait == 0
                return_string = strcat('Download not complete after wait time of ',{' '},num2str(wait_input),' seconds');
                break
            end
        end
    end

end

%%% GET KEY AND TOKEN FROM BROWNDOG USER AUTHENTICATION
function [key,token] = key_token(bdapi,uname,pword)
    options = weboptions('Username',uname,'Password',pword);
    url = strcat(bdapi,'/v1/keys');
    key = getfield(webwrite(char(url),options),'api_key');
    url = strcat(bdapi,'/v1/keys/',key,'/tokens');
    token = getfield(webwrite(char(url),options),'token');

end

% GET METADATA 
function [return_string] = getmetadata(url_string,token)
    url = java.net.URL(url_string);

    connection = url.openConnection();
    connection.setRequestMethod('GET');
    connection.setRequestProperty('Content-Type','application/json');
    connection.setDoOutput(false);
    connection.setDoInput(true);
    connection.setRequestProperty('Authorization',token);

    input = java.io.BufferedReader(java.io.InputStreamReader(connection.getInputStream()));

    line = char(input.readLine());
    return_string = line;

    while ~strcmp(line,'')    
        line = char(input.readLine());
        if ~strcmp(line,'')
            return_string = strcat(return_string,',',line);
        end
    end

end 

% POST FILE
function [file_json] = post_file(url_string,filename,input_file,token,transformation)   
    javaaddpath('./java');
 
    if strcmp(input_file(1:4),'http')
         remote = java.io.BufferedInputStream(java.net.URL(input_file).openStream());
    else
         remote = java.io.BufferedInputStream(java.io.FileInputStream(input_file));
    end
    
    stream_write = PostFile;          
    file_json = stream_write.main(remote,filename, url_string,token,transformation);
    
end


% POST URL 
function [return_string] = posturl(bd_url,file_url,token)

    url = java.net.URL(bd_url);

    connection = url.openConnection();
    connection.setRequestProperty('Authorization',token);
    connection.setRequestProperty('Content-Type','application/json');
    connection.setDoOutput(true);
    connection.setDoInput(true);

    output = connection.getOutputStream();

    body = java.lang.String(strcat('{"fileurl":"',file_url,'"}'))

    output.write(body.getBytes())
    output.flush();
    output.close();

    input = java.io.BufferedReader(java.io.InputStreamReader(connection.getInputStream()))

    line = char(input.readLine())
    return_string = line

    while ~strcmp(line,'')    
        line = char(input.readLine());
        if ~strcmp(line,'')
            return_string = strcat(return_string,',',line);
        end
    end

end






